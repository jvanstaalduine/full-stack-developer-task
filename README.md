# Full Stack Developer Task
For this task please take the screens in this Invision project that you were invited to and code them in mobile first, semantically correct HTML, SCSS and JavaScript (if needed). You will have received an email invitation to the Invision project that will step you through creating an Invision account if you don't already have one. If you did not received the invitation email then click this link ([https://invis.io/DJZNDUHMK8C](https://invis.io/DJZNDUHMK8C)), click the `Inspect` button in the bottom right corner of the screen and then click the `Request access` button.

You can use Invision's Inspect mode on each of the design screens to see the full details on fonts, font sizes, line heights, etc. as well as export any images used in the design. If you have never used Invision before, full documentation for Inspect is here ([https://support.invisionapp.com/hc/en-us/sections/360007928472-Inspect](https://support.invisionapp.com/hc/en-us/sections/360007928472-Inspect)). Please complete as much as you can in 6-8 hours.

Designs for desktop, tablet and mobile breakpoints are provided so please make your code responsive. You can use a framework such as Bootstrap if you would like. Please provide the source SCSS files and well as the compiled CSS files so that the task can be reviewed quickly. Please submit your task as a Git repository with the commit history showing your development workflow. Also include a brief overview of your challenge including highlights, decisions made and bonus items completed.

### Bonus
- Include any build scripts used in the task.
- Implement accessibility best practices so that code meets the WCAG 2.0 A or AA standard.
- Provide documentation of cross-browser testing.
- Add SEO and Lighthouse optimizations.
